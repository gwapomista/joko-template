import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.fragment.app.commit
import com.joko.ultimate.template.R

fun AppCompatActivity.reset(viewId: Int, fragment: Fragment) {
    supportFragmentManager.commitNew {
        setChild(viewId, fragment)
    }
}

fun AppCompatActivity.set(viewId: Int, fragment: Fragment) {
    supportFragmentManager.commit {
        setChild(viewId, fragment)
    }
}

fun AppCompatActivity.add(viewId: Int, fragment: Fragment) {
    supportFragmentManager.commit {
        addChild(viewId, fragment)
    }
}

fun FragmentTransaction.setChild(container: Int, fragment: Fragment) {
    setCustomAnimations(
        R.anim.ekyc_push_enter_right_to_left,
        R.anim.ekyc_push_exit_right_to_left,
        R.anim.push_enter_left_to_right,
        R.anim.ekyc_push_exit_left_to_right
    )
    replace(container, fragment)
}

fun FragmentTransaction.addChild(container: Int, fragment: Fragment) {
    setCustomAnimations(
        R.anim.ekyc_push_enter_right_to_left,
        R.anim.ekyc_push_exit_right_to_left,
        R.anim.push_enter_left_to_right,
        R.anim.ekyc_push_exit_left_to_right
    )
    replace(container, fragment)
    addToBackStack(null)
}

inline fun FragmentManager.commitNew(body: FragmentTransaction.() -> Unit) {
    repeat(backStackEntryCount) {
        popBackStack()
    }

    val transaction = beginTransaction()
    transaction.body()
    transaction.commit()
}