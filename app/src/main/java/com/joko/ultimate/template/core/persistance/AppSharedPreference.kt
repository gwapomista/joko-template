package com.joko.ultimate.template.core.persistance

import android.content.SharedPreferences
import androidx.core.content.edit

const val KEY_APP_ID = "KEY_APP_ID"

class AppSharedPreference(private val sharedPreferences: SharedPreferences) {
    var appId: String
        get() = sharedPreferences.getString(KEY_APP_ID, null) ?: KEY_APP_ID
        set(value) = sharedPreferences.edit {
            putString(KEY_APP_ID, value)
        }
}