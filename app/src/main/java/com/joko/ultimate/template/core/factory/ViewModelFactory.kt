package com.joko.ultimate.template.core.factory

import MainViewModel
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.joko.ultimate.template.core.api.ApiInterface
import com.joko.ultimate.template.core.persistance.AppSharedPreference
import io.reactivex.Scheduler
import java.security.InvalidParameterException

class ViewModelFactory(
    private val apiInterface: ApiInterface,
    private val mainScheduler: Scheduler,
    private val sharedPreferences: AppSharedPreference
) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T =
        when (modelClass) {
            MainViewModel::class.java -> MainViewModel(
                mainScheduler = mainScheduler,
                apiInterface = apiInterface,
                sharedPreferences = sharedPreferences
            )
            else -> throw InvalidParameterException(modelClass.simpleName)
        } as T
}