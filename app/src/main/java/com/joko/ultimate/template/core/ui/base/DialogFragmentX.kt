package com.joko.ultimate.template.core.ui.base

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.InsetDrawable
import android.os.Bundle
import android.view.*
import androidx.annotation.LayoutRes
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import com.joko.ultimate.template.R

open class DialogFragmentX(
    @LayoutRes private val contentLayoutId: Int
) : DialogFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        dialog!!.requestWindowFeature(STYLE_NO_TITLE)
        dialog!!.window!!.setBackgroundDrawable(InsetDrawable(ColorDrawable(Color.TRANSPARENT), 20))

        return inflater.inflate(contentLayoutId, container, false)
    }
}

fun DialogFragment.show(fragmentManager: FragmentManager) = show(fragmentManager, null)


fun DialogFragment.enableFullScreen(backgroundColor : Int = Color.WHITE){
    val window: Window = dialog!!.window!!
    dialog!!.window!!.setBackgroundDrawable(InsetDrawable(ColorDrawable(backgroundColor), 0))
    window.attributes.windowAnimations = R.style.Base_CardView
    window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
}

fun DialogFragment.enableFullScreenNoAnimation(backgroundColor : Int = Color.WHITE){
    val window: Window = dialog!!.window!!
    dialog!!.window!!.setBackgroundDrawable(InsetDrawable(ColorDrawable(backgroundColor), 0))
    window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
}