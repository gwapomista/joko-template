package com.joko.ultimate.template.core.api

import io.reactivex.Observable
import okhttp3.MultipartBody
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part
import retrofit2.http.Query

interface ApiInterface {

    @Multipart
    @POST("v2/customer/reupload-signature-image")
    fun sampleMultipart(@Query("referenceId") referenceId: String,
                               @Part file: MultipartBody.Part): Observable<Boolean>

    @POST("v2/customer/reupload-signature-image")
    fun samplePost(@Query("referenceId") referenceId: String): Observable<Boolean>

}