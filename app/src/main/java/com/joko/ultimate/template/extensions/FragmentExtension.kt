package com.peleven.kyc_jumio.utils.extensions

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.core.content.FileProvider
import androidx.core.content.getSystemService
import androidx.fragment.app.Fragment
import java.io.File
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

inline fun <reified T> Fragment.argument() = object : ReadWriteProperty<Any, T> {

    override fun getValue(thisRef: Any, property: KProperty<*>): T =
        arguments?.get(property.name) as T

    override fun setValue(thisRef: Any, property: KProperty<*>, value: T) {
        getOrCreateArguments().put(property.name, value)
    }

    private fun getOrCreateArguments() = arguments ?: Bundle().also(::setArguments)
}

fun Fragment.hideKeyboard() {
    requireContext()
        .getSystemService<InputMethodManager>()
        ?.hideSoftInputFromWindow(view?.windowToken, 0)
}

fun Fragment.showKeyboard(view: View) {
    view.post {
        view.requestFocus()
        (requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
            .showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
    }
}

fun Fragment.openShare(message: String) {
    val intent = Intent().apply {
        action = Intent.ACTION_SEND
        type = "text/plain"
        putExtra(Intent.EXTRA_TEXT, message)
    }

    val shareIntent = Intent.createChooser(intent, null)
    startActivity(shareIntent)
}

fun Fragment.openFile(file: File) {
    val uri = FileProvider.getUriForFile(
        requireContext(),
        requireActivity().applicationContext.packageName,
        file
    )

    val intent = Intent(Intent.ACTION_VIEW).apply {
        data = uri
        flags = (Intent.FLAG_GRANT_READ_URI_PERMISSION)
    }
    val intentCreateChooser = Intent.createChooser(intent, "Open File")
    startActivity(intentCreateChooser)
}

fun Fragment.openLink(link: String) {
    startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(link)))
}