import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.joko.ultimate.template.core.api.ApiInterface
import com.joko.ultimate.template.core.persistance.AppSharedPreference
import io.reactivex.Scheduler
import io.reactivex.disposables.Disposables
import io.reactivex.rxkotlin.subscribeBy
import retrofit2.HttpException
import java.io.IOException

class MainViewModel(
    private val apiInterface: ApiInterface,
    private val mainScheduler: Scheduler,
    private val sharedPreferences: AppSharedPreference
) : ViewModel() {

    val resultLive = MutableLiveData<Result>()

    private var disposable = Disposables.disposed()

    private var retryAction = {}

    override fun onCleared() = disposable.dispose()

    fun retry() = retryAction()

    fun sampleApiCall() {
        retryAction = { sampleApiCall() }
        disposable = apiInterface.samplePost(sharedPreferences.appId)
            .observeOn(mainScheduler)
            .subscribeBy(
                onNext = { resultLive.value = Success() },
                onError = ::onError
            )
    }

    private fun onError(t: Throwable) {
        resultLive.value = when (t) {
            is IOException -> {
                NetworkError()
            }
            is HttpException -> {
                ServerError(t.message())
            }
            else -> UnknownError()
        }
    }
}


interface Result

class Success : Result
class ServerError(val message: String) : Result
class NetworkError : Result
class UnknownError : Result