package com.peleven.kyc_jumio.common

import android.content.Context
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModel
import com.joko.ultimate.template.core.App
import com.joko.ultimate.template.core.AppComponent

inline val Context.appComponent: AppComponent
    get() = (applicationContext as App).getApplicationComponent()

inline val Fragment.appComponent: AppComponent
    get() = requireContext().appComponent

inline fun <reified T : ViewModel> AppCompatActivity.viewModel() = viewModels<T> {
    appComponent.getViewModelFactory()
}

inline fun <reified T : ViewModel> Fragment.viewModel() = viewModels<T> {
    appComponent.getViewModelFactory()
}
