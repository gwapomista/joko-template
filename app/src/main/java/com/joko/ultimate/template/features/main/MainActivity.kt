package com.joko.ultimate.template.features.main

import MainViewModel
import add
import reset
import set
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.joko.ultimate.template.R
import com.joko.ultimate.template.databinding.ActivityMainBinding
import com.peleven.kyc_jumio.common.viewModel
import com.peleven.kyc_jumio.utils.extensions.viewBinding


class MainActivity : AppCompatActivity(), Main {

    private val binding by viewBinding(ActivityMainBinding::inflate)

    private val viewModel by viewModel<MainViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewModel.resultLive.observe(this) {

        }
    }

    override fun addFragment(fragment: Fragment) = add(binding.container.id, fragment)

    override fun setFragment(fragment: Fragment) = set(binding.container.id, fragment)

    override fun resetFragment(fragment: Fragment) = reset(binding.container.id, fragment)

    override fun back() = onBackPressed()

    override fun onBackPressed() {
        when (supportFragmentManager.backStackEntryCount) {
            0 -> {
                super.onBackPressed()
            }
            else -> {
                supportFragmentManager.popBackStack()
            }
        }
    }
}

interface Main {
    fun back()
    fun setFragment(fragment: Fragment)
    fun addFragment(fragment: Fragment)
    fun resetFragment(fragment: Fragment)
}