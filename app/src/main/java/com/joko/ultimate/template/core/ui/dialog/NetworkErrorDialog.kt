package com.peleven.kyc_jumio.common.ui.dialog

import android.graphics.Color
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.joko.ultimate.template.R
import com.joko.ultimate.template.core.ui.base.DialogFragmentX
import com.joko.ultimate.template.core.ui.base.enableFullScreen
import com.joko.ultimate.template.databinding.LayoutDialogConnectionErrorBinding
import com.peleven.kyc_jumio.utils.extensions.viewBinding

class NetworkErrorDialog : DialogFragmentX(R.layout.layout_dialog_connection_error) {

    private val parent by lazy { (parentFragment ?: activity) as Parent }

    private val binding by viewBinding(LayoutDialogConnectionErrorBinding::bind)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        isCancelable = false

        enableFullScreen(backgroundColor = Color.TRANSPARENT)

        binding.buttonRetry.setOnClickListener {
            parent.onRetry()
            dismiss()
        }

        binding.buttonOk.setOnClickListener {
            dismiss()
        }
    }

    interface Parent {
        fun onRetry()
    }
}

fun FragmentActivity.showNetworkErrorDialog() {
    NetworkErrorDialog().show(supportFragmentManager, null)
}

fun Fragment.showNetworkErrorDialog() {
    NetworkErrorDialog().show(childFragmentManager, null)
}