package com.joko.ultimate.template.core.factory

import com.joko.ultimate.template.core.api.ApiInterface
import com.joko.ultimate.template.core.persistance.AppSharedPreference
import dagger.Module
import dagger.Provides
import io.reactivex.android.schedulers.AndroidSchedulers

@Module
class ViewModelFactoryModule {

    @Provides
    fun provide(
        apiInterface: ApiInterface,
        sharedPreferences: AppSharedPreference
    ): ViewModelFactory {
        return ViewModelFactory(
            apiInterface,
            AndroidSchedulers.mainThread(),
            sharedPreferences
        )
    }
}