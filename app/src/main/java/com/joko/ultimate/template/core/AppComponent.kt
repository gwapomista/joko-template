package com.joko.ultimate.template.core

import com.joko.ultimate.template.core.api.ApiModule
import com.joko.ultimate.template.core.factory.ViewModelFactory
import com.joko.ultimate.template.core.factory.ViewModelFactoryModule
import com.joko.ultimate.template.core.persistance.AppSharedPreference
import com.peleven.kyc_jumio.common.AppModule
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(modules = [AppModule::class, ApiModule::class, ViewModelFactoryModule::class])

interface AppComponent {
    fun getViewModelFactory(): ViewModelFactory
    fun getSharedPreference() : AppSharedPreference
}