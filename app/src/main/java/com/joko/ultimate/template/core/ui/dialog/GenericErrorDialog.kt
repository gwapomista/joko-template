package com.peleven.kyc_jumio.common.ui.dialog

import android.graphics.Color
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.joko.ultimate.template.R
import com.joko.ultimate.template.core.ui.base.DialogFragmentX
import com.joko.ultimate.template.core.ui.base.enableFullScreen
import com.joko.ultimate.template.databinding.LayoutDialogGenericErrorBinding
import com.peleven.kyc_jumio.utils.extensions.viewBinding

class GenericErrorDialog : DialogFragmentX(R.layout.layout_dialog_generic_error) {

    private val binding by viewBinding(LayoutDialogGenericErrorBinding::bind)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        isCancelable = false

        enableFullScreen(backgroundColor = Color.TRANSPARENT)

        binding.buttonOk.setOnClickListener {
            dismiss()
        }
    }
}

fun FragmentActivity.showGenericErrorDialog() {
    GenericErrorDialog().show(supportFragmentManager, null)
}

fun Fragment.showGenericErrorDialog() {
    GenericErrorDialog().show(childFragmentManager, null)
}