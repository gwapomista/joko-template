package com.joko.ultimate.template.core

import android.app.Application
import com.joko.ultimate.template.core.api.ApiModule
import com.peleven.kyc_jumio.common.AppModule

class App : Application() {

    private var appComponent: AppComponent? = null

    override fun onCreate() {
        super.onCreate()

        appComponent = DaggerAppComponent
            .builder()
            .appModule(AppModule(this))
            .apiModule(ApiModule())
            .build()
    }

    fun getApplicationComponent(): AppComponent {
        return appComponent ?: DaggerAppComponent
            .builder()
            .appModule(AppModule(this))
            .apiModule(ApiModule())
            .build()
    }
}