package com.peleven.kyc_jumio.common.ui.dialog

import android.graphics.Color
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.joko.ultimate.template.R
import com.joko.ultimate.template.core.ui.base.DialogFragmentX
import com.joko.ultimate.template.core.ui.base.enableFullScreen
import com.joko.ultimate.template.databinding.LayoutDialogNetworkErrorBinding
import com.peleven.kyc_jumio.utils.extensions.argument
import com.peleven.kyc_jumio.utils.extensions.viewBinding

class ServerErrorDialog() : DialogFragmentX(R.layout.layout_dialog_network_error) {

    constructor(message: String) : this() {
        this.message = message
    }

    private var message by argument<String>()

    private val parent by lazy { (parentFragment ?: activity) as Parent }

    private val binding by viewBinding(LayoutDialogNetworkErrorBinding::bind)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        isCancelable = false

        enableFullScreen(backgroundColor = Color.TRANSPARENT)

        binding.buttonOk.setOnClickListener {
            dismiss()
        }

        binding.buttonRetry.setOnClickListener {
            parent.onTryAgain()
            dismiss()
        }

        binding.title.text = getString(R.string.error_server)
        binding.message.text = message
    }

    interface Parent {
        fun onTryAgain()
    }
}

fun FragmentActivity.showServerErrorDialog(error: String) {
    ServerErrorDialog(error).show(supportFragmentManager, null)
}

fun Fragment.showServerErrorDialog(error: String) {
    ServerErrorDialog(error).show(childFragmentManager, null)
}